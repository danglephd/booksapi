using BooksApi.Domain.Models;
using BooksApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PcardsController : ControllerBase
    {
        private readonly BookService _bookService;

        public PcardsController(BookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        public ActionResult<List<Pcard>> Get() =>
            _bookService.GetPCards();

        [HttpGet("{id:length(24)}", Name = "GetPcard")]
        public ActionResult<Pcard> Get(string id)
        {
            var Pcard = _bookService.GetPCards(id);

            if (Pcard == null)
            {
                return NotFound();
            }

            return Pcard;
        }

        [HttpPost]
        public ActionResult<Pcard> Create(Pcard book)
        {
            _bookService.Create(book);

            return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Pcard bookIn)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Update(id, bookIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Remove(book.Id);

            return NoContent();
        }
    }
}